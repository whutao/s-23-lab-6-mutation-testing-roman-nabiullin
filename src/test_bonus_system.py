from bonus_system import calculateBonuses

_ = [
    PROGRAM_STANDARD, PROGRAM_PREMIUM, PROGRAM_DIAMOND, PROGRAM_NONSENSE
] = [
    "Standard", "Premium", "Diamond", "aboba"
]

def rounded_calculate_bonuses(program, amount):
    return round(calculateBonuses(program, amount), 2)

def test_bonuses_calculation_for_standard_program():
    assert rounded_calculate_bonuses(PROGRAM_STANDARD, 0) == 0.5
    assert rounded_calculate_bonuses(PROGRAM_STANDARD, 1) == 0.5
    assert rounded_calculate_bonuses(PROGRAM_STANDARD, 9999) == 0.5
    assert rounded_calculate_bonuses(PROGRAM_STANDARD, 10000) == 0.75
    assert rounded_calculate_bonuses(PROGRAM_STANDARD, 10001) == 0.75
    assert rounded_calculate_bonuses(PROGRAM_STANDARD, 49999) == 0.75
    assert rounded_calculate_bonuses(PROGRAM_STANDARD, 50000) == 1.0
    assert rounded_calculate_bonuses(PROGRAM_STANDARD, 50001) == 1.0
    assert rounded_calculate_bonuses(PROGRAM_STANDARD, 99999) == 1.0
    assert rounded_calculate_bonuses(PROGRAM_STANDARD, 100000) == 1.25
    assert rounded_calculate_bonuses(PROGRAM_STANDARD, 100001) == 1.25
    assert rounded_calculate_bonuses(PROGRAM_STANDARD, 999999) == 1.25

def test_bonuses_calculation_for_premium_program():
    assert rounded_calculate_bonuses(PROGRAM_PREMIUM, 0) == 0.1
    assert rounded_calculate_bonuses(PROGRAM_PREMIUM, 1) == 0.1
    assert rounded_calculate_bonuses(PROGRAM_PREMIUM, 9999) == 0.1
    assert rounded_calculate_bonuses(PROGRAM_PREMIUM, 10000) == 0.15
    assert rounded_calculate_bonuses(PROGRAM_PREMIUM, 10001) == 0.15
    assert rounded_calculate_bonuses(PROGRAM_PREMIUM, 49999) == 0.15
    assert rounded_calculate_bonuses(PROGRAM_PREMIUM, 50000) == 0.2
    assert rounded_calculate_bonuses(PROGRAM_PREMIUM, 50001) == 0.2
    assert rounded_calculate_bonuses(PROGRAM_PREMIUM, 99999) == 0.2
    assert rounded_calculate_bonuses(PROGRAM_PREMIUM, 100000) == 0.25
    assert rounded_calculate_bonuses(PROGRAM_PREMIUM, 100001) == 0.25
    assert rounded_calculate_bonuses(PROGRAM_PREMIUM, 999999) == 0.25

def test_bonuses_calculation_for_diamond_program():
    assert rounded_calculate_bonuses(PROGRAM_DIAMOND, 0) == 0.2
    assert rounded_calculate_bonuses(PROGRAM_DIAMOND, 1) == 0.2
    assert rounded_calculate_bonuses(PROGRAM_DIAMOND, 9999) == 0.2
    assert rounded_calculate_bonuses(PROGRAM_DIAMOND, 10000) == 0.3
    assert rounded_calculate_bonuses(PROGRAM_DIAMOND, 10001) == 0.3
    assert rounded_calculate_bonuses(PROGRAM_DIAMOND, 49999) == 0.3
    assert rounded_calculate_bonuses(PROGRAM_DIAMOND, 50000) == 0.4
    assert rounded_calculate_bonuses(PROGRAM_DIAMOND, 99999) == 0.4
    assert rounded_calculate_bonuses(PROGRAM_DIAMOND, 50001) == 0.4
    assert rounded_calculate_bonuses(PROGRAM_DIAMOND, 100000) == 0.5
    assert rounded_calculate_bonuses(PROGRAM_DIAMOND, 100001) == 0.5

def test_bonuses_calculation_for_nonsense_program():
    assert rounded_calculate_bonuses(PROGRAM_NONSENSE, 0) == 0.0
    assert rounded_calculate_bonuses(PROGRAM_NONSENSE, 1) == 0.0
    assert rounded_calculate_bonuses(PROGRAM_NONSENSE, 9999) == 0.0
    assert rounded_calculate_bonuses(PROGRAM_NONSENSE, 10000) == 0.0
    assert rounded_calculate_bonuses(PROGRAM_NONSENSE, 10001) == 0.0
    assert rounded_calculate_bonuses(PROGRAM_NONSENSE, 49999) == 0.0
    assert rounded_calculate_bonuses(PROGRAM_NONSENSE, 50000) == 0.0
    assert rounded_calculate_bonuses(PROGRAM_NONSENSE, 50001) == 0.0
    assert rounded_calculate_bonuses(PROGRAM_NONSENSE, 99999) == 0.0
    assert rounded_calculate_bonuses(PROGRAM_NONSENSE, 100000) == 0.0
    assert rounded_calculate_bonuses(PROGRAM_NONSENSE, 100001) == 0.0
    assert rounded_calculate_bonuses(PROGRAM_NONSENSE, 999999) == 0.0
